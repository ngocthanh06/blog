export default{
    state: {
        category: [],
        post: []
    },
    //Xử lý các thao tác chức năng
    getters:{
        getCategory(state){
            return state.category;
        },
        getPost(state){
            return state.post;
        }
    },
    // Diễn tả 1 hành động
    actions:{
        allCategory(context){
            axios.get('/api/all-category').then((res)=>{
                context.commit('categories',res.data);
            })
        },
        AllPost(context){
            axios.get('/getAll').then((res)=>{
                context.commit('posts', res.data);
            })
        }
    } ,
    //Trạng thái không thể thay đổi trực tiếp mà chỉ thay đổi thông qua commit 
    //Từ action thay đổi gọi xuống commit của mutations thông qua context.commit
    mutations: {
        categories(state, data){
            state.category = data
        },
        posts(state,payload){
            state.post = payload
        }
    }

}