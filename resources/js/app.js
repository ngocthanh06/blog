//import
import route from './Assets';
import {filter} from './filter';
require('./bootstrap');
window.Vue = require('vue');
//Editor
import 'v-markdown-editor/dist/index.css';
import Editor from 'v-markdown-editor'
Vue.use(Editor);


//vue x
import vuex from 'vuex';
Vue.use(vuex);
import storeData from './store';
const store = new vuex.Store(storeData);

// v-form
import {Form, HasError, AlertError} from 'vform';

//sweet alert
import swal from 'sweetalert2'

window.swal = swal;

const toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', swal.stopTimer)
      toast.addEventListener('mouseleave', swal.resumeTimer)
    }
  })

window.toast = toast;


//form
window.Form = Form;
Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);


Vue.component('AdminMaster', require('./components/admin/AdminMaster').default);

const app = new Vue({
    el: '#app',
    router: route,
    store
});
