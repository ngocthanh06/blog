import vue from 'vue';
import Router from 'vue-router';
import AdminHome from '../components/admin/AdminHome';
import CategoryList from '../components/admin/Category/List';
import AddCategory from '../components/admin/Category/New';
import EditCategory from '../components/admin/Category/Edit';
import PostList from '../components/admin/Posts/List';
import AddPost from '../components/admin/Posts/Add';
import EditPost from '../components/admin/Posts/Edit';
vue.use(Router);

export default new Router ({
    routes: [
        {path: '/home', component: AdminHome},
        {path: '/category-list', component: CategoryList},
        {path: '/add-category', component: AddCategory},
        {path: '/edit-category/:id', component: EditCategory},
        //post
        {path: '/post-list', component: PostList},
        {path: '/add-post', component: AddPost},
        {path: '/edit-post/:id', component: EditPost}

    ],
    mode: 'history'
})