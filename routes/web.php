<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('post','PostController@index');

//category
Route::get('deleteCategory/{id}','CategoryController@delete');
Route::get('editcategory/{id}', 'CategoryController@edit');
Route::post('updateCategory/{id}','CategoryController@update');

//post
Route::get('getAll','PostController@getAll');
Route::post('savepost','PostController@save_post');
Route::get('editpost/{id}','PostController@edit_post');
Route::get('deletePost/{id}','PostController@delete_Post');
Route::post('UpdatePost/{id}', 'PostController@Update_Post');

//Path nên dược đặt dưới cùng
Route::any('{path}', 'HomeController@index')->where(['path' => '.*']);





