<?php

namespace App\Http\Controllers;
use App\Posts;
use App\Category;
use App\User;
use Illuminate\Http\Request;
use Image;
use Auth;

class PostController extends Controller
{
    // public function index(){
    //     return Category::with('Posts')->get();
    // }

    public function getAll(){
        return Posts::with('Category','User')->get();
    }

    public function save_post(Request $request){
        $this->validate($request, [
            'title' => 'min:5|required',
            'description' => 'required',
            'cate_id' => 'required',
            'photos' => 'required',
        ]);
        //Đến số ký tự trong chuỗi photos từ dấu ';' trở ngược về trước
        $strpos= strpos($request->photos, ';');
        //cắt chuỗi trong chuỗi photos từ vị trí 0 đến số lượng của $strpos
        $sub = substr($request->photos, 0, $strpos);
        //Cắt chuỗi thành mảng ngay tại vị trí có dấu '/'
        $ex = explode('/', $sub)[1];
        //Đặt tên xử lý hình ảnh qua hàm time()
        $name = time().'.'.$ex;
        //Tạo size ảnh
        $img = Image::make($request->photos)->resize(200,200);
        //Lưu trữ hình ảnh
        $upload_path = public_path()."/uploadImage/";
        $img->save($upload_path.$name);

        $posts = new Posts();
        $posts->title = $request->title;
        $posts->description = $request->description;
        $posts->cate_id = $request->cate_id;
        $posts->user_id = Auth::user()->id;
        $posts->photos = $name;
        $posts->save();
        return ['message' => 'done'];
    }

    public function delete_Post($id){
        $post = Posts::find($id);
        $image_path = public_path()."/uploadImage/";
        $image = $image_path.$post->photos;
        if(file_exists($image)){
            @unlink($image);
        }
        $post->delete();
        return ['message' => 'done'];
    }

    public function edit_post($id){
        return Posts::find($id);
    }

    public function Update_Post($id, Request $request){
        $posts = Posts::find($id);
        $this->validate($request, [
            'title' => 'min:5|required',
            'description' => 'required'
        ]);
        

        if($posts->photos != $request->photos){
            //Đến số ký tự trong chuỗi photos từ dấu ';' trở ngược về trước
            $strpos= strpos($request->photos, ';');
            //cắt chuỗi trong chuỗi photos từ vị trí 0 đến số lượng của $strpos
            $sub = substr($request->photos, 0, $strpos);
            //Cắt chuỗi thành mảng ngay tại vị trí có dấu '/'
            $ex = explode('/', $sub)[1];
            //Đặt tên xử lý hình ảnh qua hàm time()
            $name = time().'.'.$ex;
            //Tạo size ảnh
            $img = Image::make($request->photos)->resize(200,200);
            //Lưu trữ hình ảnh
            $upload_path = public_path()."/uploadImage/";
            $image = $upload_path.$posts->photos;
            $img->save($upload_path.$name);

            if(file_exists($image)){
                @unlink($image);
            }
        }
        else{
            $name=$posts->photos;
        }
        $posts->title = $request->title;
        $posts->description = $request->description;
        $posts->cate_id = $request->cate_id;
        $posts->user_id = Auth::user()->id;
        $posts->photos = $name;
        $posts->save();
        return ['message' => 'done'];
    }
}
