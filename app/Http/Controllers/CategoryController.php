<?php

namespace App\Http\Controllers;
use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function add_category(Request $request){
        $this->validate($request, [
            'cate_name' => 'min:5|required',
        ]);
        $category = new Category();
        $category['cate_name'] = $request->cate_name;
        $category->save();
        return ['message' => 'done'];
        
    }
    // get all
    public function allCategory(){
        return Category::all();
    }
    // edit
    public function edit($id){
        return Category::find($id);
    }
    //update
    public function update($id,Request $request){
        $this->validate($request, [
            'cate_name' => 'min:5|required',
        ]);
        $category = Category::find($id);
        $category->cate_name = $request->cate_name;
        $category->save();
        return ['message' => 'success'];
    }


    // delete
    public function delete($id){
        $category = Category::find($id);
        $category->delete();
        return ['message' => 'done'];
    }
}
